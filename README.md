Smash.gg event next match finder
--------------------------------

This application queries smash.gg API to parse next match out of tournament
brackets. Uses basic GraphQL queries through POST method to 
'https://api.smash.gg/gql/alpha'.

Setup
-----
Requires Python version >3.6.

All requirements are written in `/requirements` folder. It's recommended 
installing all prereqs using:
```
pipenv install -e .
```

Running 
-------
Call the module using CLI.

```
Usage: __main__.py [OPTIONS]

Options:
  -t, --time-interval FLOAT  Time in seconds between updates.
  --output-file TEXT         Output file path. Default `available_sets.json`.
  --api_key TEXT             smash.gg API key.  [required]
  --event_id TEXT            smash.gg event id. Found in URL.  [required]
  --help                     Show this message and exit.
```



