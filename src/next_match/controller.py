import json
import logging
import time

from next_match.nextmatchfinder import NextMatchFinder

logger = logging.getLogger('next_match.controller')


class Controller:
    """
    Controller class.

    Maintains update loop and file output logic.
    """
    def __init__(self, output_file: str, update_interval: float):
        self.output_file = output_file
        self.update_interval = update_interval
        self.data_finder = None

    def dump_dict_as_json_to_file(self, data: dict):
        with open(self.output_file, 'w') as f:
            logger.info(f'Dumping data to {self.output_file}.')
            json.dump(data, f)

    def loop(self):
        while True:
            data = self.data_finder.get_data()
            self.dump_dict_as_json_to_file(data)
            logger.info(f"Sleeping for {self.update_interval} seconds.")
            time.sleep(self.update_interval)

    def init_next_match_finder(self, api_key: str, event_id: int):
        self.data_finder = NextMatchFinder(api_key=api_key, event_id=event_id)
        logger.info(
            f'Init main data update loop with interval {self.update_interval}',
        )
        self.loop()

