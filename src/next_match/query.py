import logging

import requests
import glom

logger = logging.getLogger('next_match.query')


class SmashQuery:
    """
    smash.gg query class.

    Responsible for creating GraphQL queries and posting them to smash.gg API.
    """
    def __init__(self, api_key: str, event_id: int):
        self.api_key = api_key
        self.event_id = event_id
        self.smash_url = 'https://api.smash.gg/gql/alpha'

    @staticmethod
    def _generate_payload(
            query: str,
            variables: str = None
    ) -> dict:
        if variables is None:
            variables = {}
        return {
            'query': f'{query}',
            'variables': f'{variables}'
        }

    def _post(self, query: str, variables: str) -> dict:
        payload = self._generate_payload(query=query, variables=variables)
        headers = {'Authorization': f'Bearer {self.api_key}'}
        query_url = f"{self.smash_url}"
        logger.info(f'Posting to smash API. {payload["variables"]}')
        response = requests.post(query_url, data=payload, headers=headers)
        res_json = response.json()
        logger.debug(f'{res_json}')
        return res_json

    def _get_paged_data(
            self,
            per_page: int,
            query: str,
            data_key: str
    ) -> list:
        page = 1
        variables = f'''
            {{
                "event_id": {self.event_id},
                "page": {page},
                "per_page": {per_page}
            }}
        '''
        res_json = self._post(query, variables)
        total_pages = glom.glom(
            res_json,
            data_key + '.pageInfo.totalPages',
        )
        data = glom.glom(res_json, data_key + '.nodes')
        if total_pages > 1:
            for page in range(2, total_pages + 1):
                variables = (
                    f'{{'
                    f'"event_id": {self.event_id},'
                    f'"page": {page},'
                    f'"per_page": {per_page}'
                    f'}}'
                )
                res_json = self._post(query, variables)
                data += glom.glom(res_json, data_key + '.nodes')
        return data

    def get_entrants(self) -> dict:
        logger.info('Getting entrants dict.')
        query = '''
            query getEntrants($event_id: ID!, $page: Int!, $per_page: Int!){
                event(id: $event_id) {
                    id entrants(query: {
                        page: $page, 
                        perPage: $per_page
                    }) {
                        pageInfo {
                            totalPages 
                        }
                        nodes {
                            id
                            name
                        }
                    }
                }
            }
        '''
        data_key = 'data.event.entrants'
        entrants = self._get_paged_data(
            per_page=16,
            query=query,
            data_key=data_key,
        )
        return {entrant['id']: entrant['name'] for entrant in entrants}

    def get_sets(self) -> list:
        logger.info('Getting sets.')
        query = '''
            query EventSets($event_id: ID!, $page: Int!, $per_page: Int!) {
                event(id: $event_id) {
                    sets(
                        page: $page
                        perPage: $per_page
                        sortType: MAGIC
                    ) {
                        pageInfo {
                            totalPages
                        }
                        nodes {
                            slots {
                                entrant {
                                    name
                                }
                            }
                        state
                        }
                    }
                }
            }
        '''
        data_key = 'data.event.sets'
        sets = self._get_paged_data(
            per_page=16,
            query=query,
            data_key=data_key,
        )
        return sets
