import logging

from next_match.query import SmashQuery

logger = logging.getLogger('next_match.nextmatchfinder')


class NextMatchFinder:
    """
    Next match finder class.

    This class acts as a middle man for parsing available matches.
    """
    def __init__(
            self,
            api_key: str,
            event_id: int,
    ):
        self.api_key = api_key
        self.event_id = event_id
        self.query_engine = SmashQuery(api_key=api_key, event_id=event_id)

    @staticmethod
    def _parse_sets(sets: list) -> dict:
        available_sets = {'available_sets': []}
        for _set in sets:
            state = _set['state']
            if state == 1:
                # FIXME this is hacky - think of something else
                player1 = _set['slots'][0]['entrant']
                player2 = _set['slots'][1]['entrant']
                if player1 and player2:
                    available_sets['available_sets'].append(
                        [
                            player1.get('name'),
                            player2.get('name'),
                        ]
                    )
        return available_sets

    def get_data(self) -> dict:
        sets = self.query_engine.get_sets()
        available_sets = self._parse_sets(sets)
        logger.info(f'Data get: {available_sets}')
        return available_sets
