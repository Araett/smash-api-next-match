import click
import logging

from next_match.controller import Controller

logging.basicConfig(
    format='%(asctime)s - [%(name)s] %(levelname)s | %(message)s',
    level=logging.INFO,
)
logger = logging.getLogger('next_match.__main__')


@click.command()
@click.option(
    '-t',
    '--time-interval',
    default=60.0,
    help='Time in seconds between updates.',
)
@click.option(
    '--output-file',
    default='available_sets.json',
    help='Output file path. Default `available_sets.json`.',
)
@click.option(
    '--api_key',
    required=True,
    help='smash.gg API key.'
)
@click.option(
    '--event_id',
    required=True,
    help='smash.gg event id. Found in URL.'
)
def main(api_key: str, event_id: str, time_interval: float, output_file: str):
    """
    Next match finder using smash.gg tournement bracket.

    Queries given event id bracket and outputs a list of available matches,
    sorted by relevancy.
    """
    try:
        logger.info('Init controller.')
        event_id = int(event_id)
        controller = Controller(
            update_interval=time_interval,
            output_file=output_file,
        )
        logger.info('Init next match finder.')
        controller.init_next_match_finder(
            api_key=api_key,
            event_id=event_id,
        )
    except ValueError:
        print("Event must be an integer.")
        exit(1)


if __name__ == '__main__':
    main()
