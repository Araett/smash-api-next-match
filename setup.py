from setuptools import setup, find_packages


def read_uncommented_lines(file_path: str):
    with open(file_path) as f:
        return [l.strip() for l in f.readlines() if not l.startswith('#')]


def requirements() -> list:
    return read_uncommented_lines('requirements/prod.txt')


setup(
    name='next-match',
    version="0.2.0",
    description='smash.gg event next match finder',
    license='MIT',
    packages=find_packages(where='src'),
    package_dir={'': 'src'},
    entry_points={
        'console_scripts': ['next_match = next_match.__main__.main']
    },
    include_package_data=True,
    install_requires=requirements(),
)
